export const ACTION_LOGIN_ATTEMPT = "[login] ATTEMPT"
export const ACTION_LOGIN_SUCCESS = "[login] SUCCESS"
export const ACTION_LOGIN_LOGOUT = "[login] LOGOUT"
export const ACTION_LOGIN_ERROR = "[login] ERROR"
export const ACTION_LOGIN_CREATE = "[login] CREATE"

export const loginAttemptAction = (credentials: string) => ({
    type: ACTION_LOGIN_ATTEMPT,
    payload: credentials
})

export const loginSuccessAction = (user: object) => ({
    type: ACTION_LOGIN_SUCCESS,
    payload: user
})

export const loginErrorAction = (error: string) => ({
    type: ACTION_LOGIN_ERROR,
    payload: error
})

export const loginLogoutAction = () => ({
    type: ACTION_LOGIN_LOGOUT
})

export const loginCreateAction = (credentials: string) => ({
    type: ACTION_LOGIN_CREATE,
    payload: credentials
})