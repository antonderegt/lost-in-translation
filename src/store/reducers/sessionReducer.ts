import { ACTION_SESSION_CLEAR, ACTION_SESSION_SET } from "../actions/sessionActions"

const initialState = {
    user: null,
    loggedIn: false
}

const sessionReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case ACTION_SESSION_SET:
            return {
                user: action.payload,
                loggedIn: true
            }
        case ACTION_SESSION_CLEAR:
            return {
                ...initialState
            }
        default:
            return state
    }
}

export default sessionReducer