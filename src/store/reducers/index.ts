import loginReducer from './loginReducer'
import sessionReducer from './sessionReducer'
import { combineReducers } from 'redux'

const appReducers = combineReducers({
    loginReducer,
    sessionReducer
})

export default appReducers