import {
    ACTION_LOGIN_ATTEMPT,
    ACTION_LOGIN_SUCCESS,
    ACTION_LOGIN_ERROR,
    ACTION_LOGIN_LOGOUT,
    ACTION_LOGIN_CREATE
} from '../actions/loginActions'

const initialState = {
    loginAttempting: false,
    loginError: ''
}

const authReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case ACTION_LOGIN_ATTEMPT:
            return {
                loginAttempting: true,
                loginError: ''
            }
        case ACTION_LOGIN_SUCCESS:
            return {
                ...initialState
            }
        case ACTION_LOGIN_ERROR:
            return {
                loginAttempting: false,
                loginError: action.payload
            }
        case ACTION_LOGIN_CREATE:
            return {
                ...state
            }
        case ACTION_LOGIN_LOGOUT:
            return { ...initialState }
        default:
            return state
    }
}

export default authReducer