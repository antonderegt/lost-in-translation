import { addUser, getUser } from "../../api/litApi";
import {
    ACTION_LOGIN_ATTEMPT,
    ACTION_LOGIN_SUCCESS,
    ACTION_LOGIN_LOGOUT,
    ACTION_LOGIN_CREATE,
    loginSuccessAction,
    loginErrorAction,
    loginCreateAction
} from "../actions/loginActions"
import { sessionSetAction, sessionClearAction } from "../actions/sessionActions";

// @ts-ignore
export const loginMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_LOGIN_ATTEMPT) {
        getUser(action.payload)
            .then(user => {
                if (user.length === 0) {
                    dispatch(loginCreateAction(action.payload))
                }
                dispatch(loginSuccessAction(user[0]))
            })
            .catch(e => {
                console.log("error 25: " + e.message);
                console.log(e);


                dispatch(loginErrorAction(e.message))
            })
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }

    if (action.type === ACTION_LOGIN_LOGOUT) {
        dispatch(sessionClearAction())
    }

    if (action.type === ACTION_LOGIN_CREATE) {
        addUser(action.payload)
            .then(user => {
                dispatch(loginSuccessAction(user))
            }).catch(e => {
                dispatch(loginErrorAction(e.message))
            })
    }


}