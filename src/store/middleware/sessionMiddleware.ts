import { loginAttemptAction } from "../actions/loginActions"
import {
    ACTION_SESSION_CLEAR,
    ACTION_SESSION_INIT,
    ACTION_SESSION_SET
} from "../actions/sessionActions"

// @ts-ignore
export const sessionMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_SESSION_INIT) {
        const storedSession = localStorage.getItem('user-session')

        if (!storedSession) {
            return
        }

        const userName = JSON.parse(storedSession)
        dispatch(loginAttemptAction(userName))
    }

    if (action.type === ACTION_SESSION_SET) {
        localStorage.setItem('user-session', JSON.stringify(action.payload.name))
    }

    if (action.type === ACTION_SESSION_CLEAR) {
        localStorage.clear()
    }
}