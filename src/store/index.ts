import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import middleware from "./middleware";
import appReducers from "./reducers";

export default createStore(
    appReducers,
    composeWithDevTools(middleware)
)