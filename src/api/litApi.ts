const devUrl = 'http://localhost:3001'
const url = 'https://lit-db-anton.herokuapp.com'

// Add a translation to the db
export function addTranslation(data: object) {
    return fetch(`${url}/translations`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(response => response.json())
}

export function fetchTranslations(userId: number) {
    return fetch(`${url}/users/${userId}/translations?deleted=false&_sort=id&_order=desc&_limit=10`)
        .then(response => response.json())
}

// Mark translation deleted (by id)
export function deleteTranslation(id: number) {
    return fetch(`${url}/translations/${id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ deleted: true }),
    })
        .then(response => response.json())
}

// Get user from db
export function getUser(name: string) {
    return fetch(`${url}/users?name=${name}`)
        .then(response => response.json())
}

// Add user to db
export function addUser(name: string) {
    return fetch(`${url}/users`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name }),
    })
        .then(response => response.json())
}