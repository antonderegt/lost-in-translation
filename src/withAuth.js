import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'

const withAuth = Component => props => {
    const { loggedIn } = useSelector(state => state.sessionReducer)

    if(loggedIn) {
        return <Component {...props} />
    } else {
        return <Redirect to="/" />
    }
}

export default withAuth