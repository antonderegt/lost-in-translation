import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'

const withNoAuth = Component => props => {
    const { loggedIn } = useSelector(state => state.sessionReducer)

    if(loggedIn) {
        return <Redirect to="/translate" />
    } else {
        return <Component {...props} />
    }
}

export default withNoAuth