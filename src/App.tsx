import './App.css'
import Logo from './assets/Logo.png'
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom'
import Home from './components/containers/Home'
import Translate from './components/containers/Translate'
import Profile from './components/containers/Profile'
import NotFound from './components/containers/NotFound'
import { useSelector, RootStateOrAny } from 'react-redux'

function App() {
  const { loggedIn, user } = useSelector((state: RootStateOrAny) => state.sessionReducer)

  return (
    <Router>
      <div className="App">
        <header className="header">
          <Link to="/" >
            <div className="header__logo">
              <div className="header__logo__image">
                <img src={Logo} alt="Logo Lost in Translation" />
              </div>
              <div className="header__logo__title">
                Lost in Translation
              </div>
            </div>
          </Link>
          <div>
            {loggedIn && <Link to="/translate">Translate</Link>}
            {loggedIn && user && <Link to="/profile">{user.name}</Link>}
          </div>
        </header>

        <main>
          <Switch>
            <Route exact={true} path="/" component={Home}></Route>
            <Route path="/translate" component={Translate}></Route>
            <Route path="/profile" component={Profile}></Route>
            <Route path="*" component={NotFound}></Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
