import './Loading.css'
import { useState, useEffect } from 'react';

const Loading = () => {
    const [value, setValue] = useState(0);

    useEffect(() => {
        setValue(100);
        // eslint-disable-next-line 
    }, [])

    return (
        <div className="loading">
            <div className="loading__bar">
                <div style={{ width: `${value}%` }} className="loading__progress" />
            </div>
        </div>
    )
}

export default Loading