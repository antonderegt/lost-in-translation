import images from '../../assets'
import { useEffect, useState } from 'react'
import './Signs.css'

const Signs = (props: any) => {

    const [translatedText, setTranslatedText] = useState([''])

    const translate = () => {
        const translated = props.text.toLowerCase().split('').map((char: string) => {
            // Images are in an array where 'a.png' is at index 0
            const letterCode = char.charCodeAt(0) - 97;
            return images[letterCode];
        })

        setTranslatedText(translated)

    }

    useEffect(() => {
        translate()
        // eslint-disable-next-line 
    }, [props.text])


    const signs = translatedText.map((char, index) => {
        return <img className="sign__image" src={char} alt={char} key={index} />
    })

    return (
        <div className="sign">
            {signs}
            <span className="sign__text">Translation</span>
        </div>
    )
}

export default Signs