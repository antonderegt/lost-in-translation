import { deleteTranslation } from '../../api/litApi'
import { useState } from "react";
import Signs from '../shared/Signs'
import './ProfileElement.css'
import Delete from '../../assets/delete.png'

const ProfileElement = (props: any) => {
    const [visible, setVisible] = useState(true);

    const onButtonClick = () => {
        setVisible(false)
        deleteTranslation(props.id)
    }

    return visible ? (
        <div className="profile-element">
            <div className="profile-element__title">
                <h3>{props.text}</h3>
                <img className="profile-element__title__delete" onClick={onButtonClick} src={Delete} alt="Delete" />
            </div>
            <Signs text={props.text} />
        </div>
    ) : <div />
}

export default ProfileElement