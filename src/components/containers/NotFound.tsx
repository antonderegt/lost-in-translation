import { useHistory } from 'react-router-dom'

const NotFound = () => {

    const history = useHistory()

    const onButtonClick = () => {
        history.push('/')
    }

    return (
        <main>
            <h1>Location unknown</h1>
            <button onClick={onButtonClick}>Go Home</button>
        </main>
    )
}

export default NotFound