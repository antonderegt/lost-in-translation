import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { loginAttemptAction } from '../../store/actions/loginActions'
import Loading from '../shared/Loading'
import Logo from '../../assets/Logo.png'
import ArrowRight from '../../assets/arrow-right.png'
import Keyboard from '../../assets/keyboard.png'
import './Home.css'
import withNoAuth from '../../withNoAuth'

const Home = () => {
  const [name, setName] = useState('');
  const [nameError, setNameError] = useState('');
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch()

  // @ts-ignore
  const { loginError, loginAttempting } = useSelector(state => state.loginReducer)

  const onButtonClick = async () => {
    setLoading(true)

    if (name === '') {
      setNameError('Please enter your name')
    } else if (name.length < 3) {
      setNameError('Names of less than three characters are fake!')
    } else {
      dispatch(loginAttemptAction(name))
    }

    setLoading(false)
  }

  const onInputChange = (event: { target: HTMLInputElement }) => {
    if (nameError !== '') {
      setNameError('')
    }

    setName(event.target.value)
  }

  const onKeyUp = (event: any) => {
    if (event.key === 'Enter') {
      onButtonClick()
    }
  }

  const styles = {
    error: {
      color: 'red'
    }
  }

  return (
    <main className="home">
      <section className="banner">
        <div className="banner__logo">
          <img src={Logo} alt="Logo Lost in Translation" />
        </div>
        <div className="banner__text">
          <h1>Lost in Translation</h1>
          <h3>Get started</h3>
        </div>
      </section>
      <section className="input-home">
        {loading && <Loading />}
        {!loading &&
          <div >
            <img className="input__image" src={Keyboard} alt="keyboard" />
            <input className="input__field" type='text' placeholder="What's your name?" onChange={onInputChange} onKeyUp={onKeyUp} />
            <img className="button__image" src={ArrowRight} alt="arrow right" onClick={onButtonClick} />
          </div>}

        {loginAttempting &&
          <p>Trying to login...</p>
        }

        {loginError &&
          <p style={styles.error}>{loginError}</p>
        }

        {nameError &&
          <p style={styles.error}>{nameError}</p>
        }

      </section>
    </main>
  )
}

export default withNoAuth(Home)