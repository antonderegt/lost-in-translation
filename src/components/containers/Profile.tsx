import withAuth from '../../withAuth'
import { fetchTranslations } from '../../api/litApi'
import { useState, useEffect } from 'react'
import ProfileElement from './ProfileElement'
import { useSelector, RootStateOrAny } from 'react-redux'
import { useDispatch } from 'react-redux'
import { loginLogoutAction } from '../../store/actions/loginActions'
import Logout from '../../assets/logout.png'
import Loading from '../shared/Loading'
import './Profile.css'

const Profile = () => {
    const { user } = useSelector((state: RootStateOrAny) => state.sessionReducer)
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch()

    const [state, setState] = useState({
        translations: [{
            id: 1,
            text: '',
            signImageIndices: []
        }]
    })

    useEffect(() => {
        setLoading(true)
        fetchTranslations(user.id)
            .then((translations: any) => {
                setState(prevState => {
                    return {
                        ...prevState,
                        translations
                    }
                })
                setLoading(() => {
                    return false
                })
            })
            .catch((error: any) => {
                console.error(error.message)
                setLoading(false)
            })
        // eslint-disable-next-line 
    }, [])

    const onButtonClick = () => {
        dispatch(loginLogoutAction())
    }

    const translationElements = state.translations.map(translation => {
        const key = "key" + translation.text + translation.id
        return (<ProfileElement text={translation.text} images={translation.signImageIndices} key={key} id={translation.id} />)
    })

    return (
        <div>
            <div className="banner banner--profile">
                <h1 className="banner__text">Your latest translations</h1>
                <div className="button">
                    <button className="logout" onClick={onButtonClick}>Logout <img src={Logout} alt="Logout" /></button>
                </div>
            </div>
            {loading && <Loading />}
            {!loading && translationElements}
        </div>
    )
}

export default withAuth(Profile)