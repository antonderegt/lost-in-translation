import { useState } from 'react'
import withAuth from '../../withAuth'
import { useSelector, RootStateOrAny } from 'react-redux'
import { addTranslation } from '../../api/litApi'
import Signs from '../shared/Signs'
import './Translate.css'
import ArrowRight from '../../assets/arrow-right.png'
import Keyboard from '../../assets/keyboard.png'

const Translate = () => {
    const { user } = useSelector((state: RootStateOrAny) => state.sessionReducer)
    const [inputText, setInputText] = useState('')
    const [inputError, setInputError] = useState('')
    const [text, setText] = useState('')

    const onButtonClick = async () => {
        // Only allow letters and spaces, other characters will be discarded
        const filteredInput = inputText.trim().replace(/[^a-zA-Z ]/gi, "")

        if (filteredInput === '') {
            // Reset props for 'Signs' child component
            setText('')
            setInputError('Please enter some text')
            return
        }

        const translation = {
            text: filteredInput,
            deleted: false,
            userId: user.id
        }

        // Set props for 'Signs' child component
        setText(filteredInput)

        try {
            await addTranslation(translation)
        } catch (e) {
            console.log(e);
        }
    }

    const onKeyUp = (event: any) => {
        if (event.key === 'Enter') {
            onButtonClick()
        }
    }

    const onInputChange = (event: any) => {
        if (inputError !== '') {
            setInputError('')
        }

        setInputText(event.target.value)
    }

    const styles = {
        error: {
            color: 'red'
        }
    }

    return (
        <section className="translate">
            <div className="input-container">
                <div className="input-translate">
                    <div >
                        <img className="input__image" src={Keyboard} alt="keyboard" />
                        <input className="input__field" type='text' placeholder="What do you want to translate?" onChange={onInputChange} onKeyUp={onKeyUp} />
                        <img className="button__image" src={ArrowRight} alt="Arrow right" onClick={onButtonClick} />
                    </div>
                </div>
            </div>

            {inputError && <p style={styles.error}>{inputError}</p> }

            {text && <Signs text={text} />}
        </section>
    )
}

export default withAuth(Translate)