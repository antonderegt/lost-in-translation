# Lost in Translation

This project helps to translate typed characters to [American Sign Language](https://www.nidcd.nih.gov/health/american-sign-language). 

## Demo
Check out the demo [here](https://lit-anton.herokuapp.com/). This demo uses a seperate heroku app to host the database, see this [repo](https://github.com/jesperorb/json-server-heroku) on how to set it up.

## Prerequisites
1. Make sure you have [Node.js](https://nodejs.org/) installed
2. Clone the project
```
git clone https://gitlab.com/antonderegt/lost-in-translation.git
```
3. Install the dependencies
```
cd lost-in-translation
npm install
```

## Run the Project
In the project directory, you can run:

### `npm run server`
Runs the `json-server` backend on port 3001.
If you want to use your own database, make sure to update the url in the file `/src/api/litApi.ts`. Otherwise this repo will use a database deployed on heroku.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Background Information
This project automatically deploys on [Heroku](https://heroku.com/) on every merge with the master branch.
### Dependencies
- React
- Redux
- React Router
- JSON Server
- TypeScript

## Additional Scripts

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
